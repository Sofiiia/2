﻿using System;

class MyProgram
{
  
    static void radixsort(int[] Array)
    {
        int n = Array.Length;
        int max = Array[0];

       
        for (int i = 1; i < n; i++)
        {
            if (max < Array[i])
                max = Array[i];
        }

       
        for (int place = 1; max / place > 0; place *= 10)
            countingsort(Array, place);
    }

    static void countingsort(int[] Array, int place)
    {
        int n = Array.Length;
        int[] output = new int[n];

       
        int[] freq = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        
        for (int i = 0; i < n; i++)
            freq[(Array[i] / place) % 10]++;

        
        for (int i = 1; i < 10; i++)
            freq[i] += freq[i - 1];

       
        for (int i = n - 1; i >= 0; i--)
        {
            output[freq[(Array[i] / place) % 10] - 1] = Array[i];
            freq[(Array[i] / place) % 10]--;
        }

       
        for (int i = 0; i < n; i++)
            Array[i] = output[i];
    }

    static void PrintArray(int[] Array)
    {
        int n = Array.Length;
        for (int i = 0; i < n; i++)
            Console.Write(Array[i] + " ");
        Console.Write("\n");
    }

   
    static void Main(string[] args)
    {
        int[] MyArray = { 170, 45, 75, 90, 802, 24, 2, 66 };
        Console.Write("Original Array\n");
        PrintArray(MyArray);

        radixsort(MyArray);
        Console.Write("\nSorted Array\n");
        PrintArray(MyArray);
    }
}